" vimrc
" Marlon van der Linde
" marlon@glov.org marlon@kbye.co.za

" ====== for starters
set nocompatible
set backspace=start,indent,eol
set viminfo=!,'100,\"100,:20,<50,s10,h,n~/.viminfo
set autoread

" command lines
set cmdheight=2
set laststatus=2

"set statusline=[%l,%v\ %P%M]\ %f\ %r%h%w\ (%{&ff})
set statusline+=%-20f\ 
set statusline+=HUGENESS:%{FileSize()},
set statusline+=enc:[%{strlen(&fenc)?&fenc:'none'},
set statusline+=format:%{&ff}],
set statusline+=mod:%m,
set statusline+=%r
set statusline+=%y
set statusline+=%=
set statusline+=col:%c,
set statusline+=line:%l-of-%L
set statusline+=\ %P


function! FileSize()
     let bytes = getfsize(expand("%:p"))
     if bytes <= 0
         return ""
     endif
     if bytes < 1024
         return bytes
     else
         return (bytes / 1024) . "K"
     endif
 endfunction

set showcmd
set scrolloff=4

" ====== Looking, Feeling, Aiding
set ttyfast
set ruler
" highlight and incremental search
set incsearch
set hlsearch
set wrapscan
set number
set t_Co=256
colorscheme ron
set mousemodel=popup
filetype plugin indent on
set gcr=a:blinkon0

" highlighting of cursor and column
set cursorline

" bold the cursorline
hi CursorLine term=bold cterm=bold
"hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
"hi CursorLine guifg=red guibg=blue
"autocmd InsertEnter * highlight CursorLine guifg=white guibg=blue ctermfg=white ctermbg=blue
"autocmd InsertLeave * highlight CursorLine guifg=white guibg=darkblue ctermfg=white ctermbg=darkblue

autocmd InsertEnter * highlight CursorLine cterm=bold term=bold
autocmd InsertLeave * highlight CursorLine cterm=NONE term=NONE

" a cursorline on enter, gone on leave
"autocmd WinEnter * setlocal cursorline
"autocmd WinLeave * setlocal nocursorline

set cursorcolumn
hi CursorColumn cterm=NONE ctermbg=darkgrey

" Cursor Props
highlight Cursor guifg=white guibg=black
highlight iCursor guifg=white guibg=steelblue
set guicursor=n-v-c:block-Cursor
set guicursor+=i:ver100-iCursor
set guicursor+=n-v-c:blinkon0
set guicursor+=i:blinkwait10


" gutter
sign define dummy
autocmd BufEnter * execute 'sign place 9999 line=1 name=dummy buffer=' . bufnr('')
" shuttup and dont blink
set noerrorbells
set vb t_vb=

" ====== more productive stuff

" Remember where the cursor is
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
" go go hidden buffers
set hidden
" Set the terminal titles 
set title
set titleold="terminal"
set titlestring=%F


" ==== file type based stuff

" xml, html, etc 
autocmd FileType xml,html,htmljinja,htmldjango setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType html,htmljinja,htmldjango imap <buffer> <c-e> <Plug>SparkupExecute
autocmd FileType html,htmljinja,htmldjango imap <buffer> <c-l> <Plug>SparkupNext
autocmd FileType html setlocal commentstring=<!--\ %s\ -->
autocmd FileType htmljinja setlocal commentstring={#\ %s\ #}
let html_no_rendering=1


" Bashrc
au BufNewFile,BufRead .bashrc*,bashrc,bash.bashrc,.bash_profile*,.bash_logout*,*.bash,*.ebuild call SetFileTypeSH("bash")
" Makefiles
au BufRead,BufNewFile Makefile* set noexpandtab
" generals for PHP
autocmd FileType php setlocal shiftwidth=4 tabstop=8 softtabstop=4 expandtab
" generals for Java
autocmd FileType java setlocal shiftwidth=2 tabstop=8 softtabstop=2 expandtab
autocmd FileType java setlocal commentstring=//\ %s
" for vim :P
autocmd FileType vim setlocal expandtab shiftwidth=2 tabstop=8 softtabstop=2
" for YAML
autocmd FileType yaml setlocal expandtab shiftwidth=2 tabstop=8 softtabstop=2
autocmd BufNewFile,BufRead *.sls setlocal ft=yaml

" python
autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8
      \ formatoptions+=croq softtabstop=4 smartindent
      \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
let python_highlight_all=1
let python_highlight_exceptions=0
let python_highlight_builtins=0
let python_slow_sync=1
autocmd FileType pyrex setlocal expandtab shiftwidth=4 tabstop=8 softtabstop=4 smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class,with

" javascript
autocmd FileType javascript setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
autocmd BufNewFile,BufRead *.json setlocal ft=javascript
autocmd FileType javascript setlocal commentstring=//\ %s
autocmd FileType javascript noremap <buffer> <leader>r :call JsBeautify()<cr>
autocmd FileType javascript let b:javascript_fold = 0
let javascript_enable_domhtmlcss=1
let g:syntastic_javascript_checkers = ['jshint'] 
let g:syntastic_javascript_jshint_args='--config ~/.vim/extern-cfg/jshint.json'

" point out bad whitespace
highlight BadWhitespace ctermbg=red guibg=red

" tabs at the start of a line in Python = BAD
" whitespace at the end = BAD
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" actually whitespace at the end of any line is bad
au BufRead,BufNewFile *.yml,*.java,*.php,*.txt match BadWhitespace /\s\+$/

" Wrap text after a certain number of characters
" Python: 79 
" C: 79
" PHP and Java and Text = 120
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h set textwidth=79
au BufRead,BufNewFile *.java,*.php,*.txt set textwidth=120


" Use UNIX (\n) line endings for new files (be friendly to existing ones)
" For python, java, php
au BufNewFile *.py,*.java,*.php set fileformat=unix


" Automatically indent based on file type: 
filetype indent on
" Keep indentation level from previous line: 
set autoindent

" Fold on indent
set foldmethod=indent

