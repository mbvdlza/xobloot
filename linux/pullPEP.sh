#!/bin/bash

# pull or clone burningToast and run lint checks on it.
# make sure to have a ssh key setup for bitbucket access

repoUrl="xxx@bitbucket.org:username/somerepo.git"
repoRoot="$HOME/tmp/"
repoPath="burningtoast"
gitPath=$(whereis -b git | awk '{print $2}')
#pylintPath=$(whereis -b pylint | awk '{print $2}')
pep8Path=$(command -v pep8)
seemsOK=false
mailRecipients="person1@email.domain"
mailContent="$HOME/tmp/mail"
testBranch="dev"
todayDate=$(date)

echo "______________ Nightly PEP8 report for $repoPath for $todayDate ___________" > $mailContent
echo " " >> $mailContent

if [ -z $pep8Path ];
then
    echo "pep8 must be installed... ciao." >> $mailContent
    exit
fi

# cehck if pep8 is available or exit

echo "trying $repoRoot$repoPath" >> $mailContent

if [ ! -d $repoRoot$repoPath ];
then
    echo "Repo: $repoRoot$repoPath does not exist... trying to clone it"; >> $mailContent
    cd $repoRoot;
    $gitPath clone $repoUrl >> $mailContent;
    seemsOK=true
    echo " " >> $mailContent
else
    echo "Repo $repoRoot$repoPath exists. Pulling..." >> $mailContent;
    cd $repoRoot$repoPath;
    $gitPath pull >> $mailContent >> $mailContent;
    seemsOK=true
    echo " " >> $mailContent
fi

if [ $seemsOK ];
then
    echo " " >> $mailContent
    echo "============ PEP8 Report Start ==========================================" >> $mailContent;
    cd $repoRoot$repoPath;
    $gitPath checkout --track -b $testBranch origin/$testBranch >> $mailContent;
    $pep8Path --exclude=*.md * >> $mailContent;
    echo "=========================================================================" >> $mailContent;
    echo " " >> $mailContent
    echo "That is all, folks." >> $mailContent
fi

cat $mailContent | /usr/bin/mail -s "PEP8 on Repo $repoPath : Bi-Nightly Report for $todayDate" $mailRecipients
