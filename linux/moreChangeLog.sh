#!/bin/bash
# marlon van der linde
# generate a changelog from git log output. Takes some optional arguments, see -h

# exit codes for callers
#  0    - had a nice day normal exit
#  5    - git could not be found
#  1    - script was not run in the context of a git repo location
#  99   - pwd returned something odd. We need this for filenames and id. Bailing

GITPATH=$(command -v git)
WHEREAMI=${PWD##*/}

if [ ! -n "$WHEREAMI" ];
then
    echo -e "Sorry! :( \nworking directory could not be determined. \nWeird shell issue. Ask Core!"
    exit 99
fi

if [ ! -n "$GITPATH" ];
then
    echo "Couldn't find a path to the git executable. :'("
    exit 5
fi

runDate=$(date +"%m-%d-%Y_%H%M_%Z")
fileName="$WHEREAMI.ChangeLog"    # default filename
consoleOutPrefix="--== "

function usage()
{
    IAmNoOne=$(basename $0)
    echo "bash $IAmNoOne takes any of the following arguments:"
    echo -e "-h \n\tPrints basic usage, this, which you are reading"
    echo -e "-n \n\tNumber of commits to output. Omitting this will not limit commits. An integer \n\teg: -n 7"
    echo -e "-s \n\tLog commits since this date. Takes a date or arbitrary string. Omitting this will get everything. \n\teg: -s \"3 weeks ago\" or -s \"2013-02-25\""
    echo -e "-o \n\tOutput filename. The default is '$fileName-$runDate' (date/time at runtime). \n\teg: -o MyChangeLogFoobars.txt"
    exit 0
}

flagNumOfCommits=""
flagFilterSince=""

while getopts hn:s:o: opt
do
    case "$opt" in
        h) usage;;
        n) flagNumOfCommits="-n $OPTARG";;
        s) flagFilterSince="--since=\"$OPTARG\"";;
        o) fileName=$OPTARG;;
    esac
done

# some pre-determined options. Put the ones we ALWAYS want here.
flagDateShort=" --date=short"
flagDateOrder=" --date-order"
flagNoMerges=" --no-merges"
fileName="$fileName-$runDate"

combinedFlags="$flagDateOrder $flagDateShort $flagNoMerges $flagNumOfCommits $flagFilterSince"
echo "$consoleOutPrefix Writing to Output File " $fileName
echo "$consoleOutPrefix git log flags in use: $combinedFlags"

# got some serious sed part of this from someone's blog...
if test -d ".git"; then
    $GITPATH log $combinedFlags | \
        sed -e '/^commit.*$/d' | \
        awk '/^Author/ {sub(/\\$/,""); getline t; print $0 t; next}; 1' | \
        sed -e 's/^Author: //g' | \
        sed -e 's/>Date:   \([0-9]*-[0-9]*-[0-9]*\)/>\t\1/g' | \
        sed -e 's/^\(.*\) \(\)\t\(.*\)/\3    \1    \2/g' > $fileName
    echo "$consoleOutPrefix Everything seems to be done, you should find the logs in $fileName"
    exit 0
else
    # this should not really happen if we git hook or ant scriptify this thing
    echo "CrystalBallException. Must be called from inside a git repo"
    exit 1
fi


