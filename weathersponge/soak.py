"""
WeatherSponge
Copyright (c) 2008 Marlon B van der Linde <marlon@mbvdl.net>
See 'COPYING' for license

"""

from WeatherSponge import WeatherSponge
from Storage import Storage
import sys

if len(sys.argv) != 2:
	sys.exit("Soak requires one argument. Only supply 'n' or 'd' or 'night' or 'day'")
slot = sys.argv[1].lower()

db = {
		"dbhost" : "your-db-hostname",
		"dbuser" : "jonnybravo",
		"dbpasswd" : "bunny",
		"dbname" : "weatherSponge"
		}

sponge = WeatherSponge("http://localhost/~marlon/dummyrss.xml", "{http://purl.org/rss/1.0/modules/content/}")
storage = Storage(db)

if not sponge or not storage:
	print "Fatal instantiation failure, exiting."
	raise SystemExit()

wData = sponge.parseWeatherData()
cleanData = sponge.cleanValues(wData)

for k, v in cleanData.iteritems():
	storage[k] = v

if slot == 'n' or slot == 'night':
	storage['slot'] = 'n'
elif slot == 'd' or slot == 'day':
	storage['slot'] = 'd'
else:
	sys.exit("Only supply 'n' or 'd' or 'night' or 'day'")

id = storage.insertRow()
if not id:
	print "insertRow() returned None - table row id: %d failed" % (id, )
	raise SystemExit()
else:
	print "Success. Row id %d inserted" % (id, )

