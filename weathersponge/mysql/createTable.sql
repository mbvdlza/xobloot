CREATE TABLE `sponge` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`pubtitle` VARCHAR( 255 ) NOT NULL ,
	`pubdate` VARCHAR( 255 ) NOT NULL ,
	`temp` VARCHAR( 10 ) NULL ,
	`windspeed` VARCHAR( 10 ) NULL ,
	`dewpoint` VARCHAR( 10 ) NULL ,
	`winddirection` VARCHAR( 10 ) NULL ,
	`visibility` VARCHAR( 10 ) NULL ,
	`humidity` VARCHAR( 10 ) NULL ,
	`windchill` VARCHAR( 10 ) NULL ,
	`heatindex` VARCHAR( 10 ) NULL ,
	`barometer` VARCHAR( 10 ) NULL ,
	`slot` VARCHAR( 10 ) NULL,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = MYISAM ;

