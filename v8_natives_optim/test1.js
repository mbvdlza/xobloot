'use strict';

var v8 = require('v8-natives');

function possibleBoom() {
    return 999;
}

var errorThing = {
    value: null
};

function handleMaybeTroubles(functi, ctx, args) {
    try {
        return functi.apply(ctx, args);
    }
    catch(err) {
        errorThing.value = err;
        return errorThing;
    }
}

var result = handleMaybeTroubles(possibleBoom);

if (result === errorThing) {
    var err = errorThing.value;
    console.log('---- ERROR ----');
    console.log(err);
} else {
    console.log('---- RESULT ----');
    console.log(result);
}



v8.helpers.printStatus(possibleBoom);
