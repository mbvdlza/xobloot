<h3>Marlon's HTML HEX increment font color chart, aka MHHIFCC :)</h3>
<br />
<table>
<?php

$r=0;
while ($r <= 15) {
    echo "\n<tr>";
    $g=0;
    while ($g <= 15) {
        echo "\n<tr>";
        $b=0;
        while ($b <= 15) {
            $bgc = dechex($r) . dechex($r) . dechex($g) .dechex($g) . dechex($b) . dechex ($b);
            echo "\n\t<td bgcolor=\"#$bgc\"><font size=1>#$bgc</font></td>";
            $b = $b+1;
        }
        $g = $g+1;

    echo "\n</tr>";
    }
    $r = $r+1;
}
?>
</table> 
