#!/usr/bin/php
<?php

/**
 * Create a rough HTML output of a unix file listing
 * m@kbye.co.za
 * 2010-01-23
 */

if($argc < 2) {
    echo "\nOne parameter needs to be supplied:\n\t_path_\n";
    exit(1);
}

$awked_whereis = `whereis -b ls | awk '{print $2}'`;
$param_ls = $argv[1];

$cmd = $awked_whereis . $param_ls;
$cmd = str_replace("\n", " ", trim($cmd));


function directree($upperdir, $deep = 0){
    $dirs = array_diff( scandir($upperdir), array( ".", ".." ) );
    $dir_array = array();
    foreach( $dirs as $d ){
        if( is_dir($upperdir."/".$d) ) {
            if($deep==1) {
                echo "\n\t\t<h3 STYLE='color:  yellow; line-height: 25px; font-size: 20px'>$d</h3>";
            } else {
                echo "\n\t\t<h1 STYLE='color:  lightgreen; line-height: 35px; font-size: 30px'>$d</h1>";
            }
            $dir_array[$d] = directree($upperdir.'/'.$d, 1 );
        } else {
            echo "\n\t\t\t<b>$d</b><br />";
            $dir_array[$d] = $d;
        }
    }
    return $dir_array;
} 

$now = date('r');
echo "<html>\n\t<head>\n\t\t<title>$argv[1]</title>";
echo "";
echo "\n\t</head>\n</html>\n<body STYLE='background-color: black; color: lightgrey; font-family: verdana, tahoma, arial, system;'>";
echo "<br /><br /><h5>Generated: $now</h5>";
$r = directree($argv[1]);
echo "\n</body>\n</html>\n";

?>
