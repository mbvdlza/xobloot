<?php

/**
 * LZW.
 * Marlon v/d Linde
 * Date: 2015/05/18
 */
class ZCompre {

    private $phraseUncompressed;
    private $phraseCompressed;
    private $dictionaryCompress;
    private $dictionaryUncompress;
    private $dictionarySize = 256;

    public function __construct() {
        $this->phraseCompressed = '';
        $this->phraseUncompressed = '';

        for ($i = 0; $i < 256; $i++) {
            $this->dictionaryCompress[chr($i)] = $i;
        }

        for ($i = 0; $i < 256; $i++) {
            $this->dictionaryUncompress[$i] = chr($i);
        }

    }

    public function compress($phrase) {
        $this->phraseUncompressed = $phrase;
        if (strlen($this->phraseUncompressed) < 1) {
            throw new RuntimeException('Cannot compress an empty string properly.');
        }
        $w = '';
        $result = array();
        for ($i = 0; $i < strlen($this->phraseUncompressed); $i++) {
            $c = $this->phraseUncompressed[$i];
            $wc = $w . $c;
            if (array_key_exists($w . $c, $this->dictionaryCompress)) {
                $w = $w . $c;
            } else {
                array_push($result, $this->dictionaryCompress[$w]);
                //$this->dictionaryCompress[$wc] = $this->dictionarySize++;
                $w = (string)$c;
            }
        }
        if ($w !== "") {
            array_push($result, $this->dictionaryCompress[$w]);
        }
        return implode(",", $result);
    }

    public function decompress($phrase) {
        $this->phraseCompressed = $phrase;
        if (strlen($this->phraseUncompressed) < 1) {
            throw new RuntimeException('Cannot compress an empty string properly.');
        }

        $this->phraseCompressed = explode(",", $this->phraseCompressed);

        $entry = '';
        $this->dictionarySize = 256;
        $w = chr($this->phraseCompressed[0]);
        $result = $w;

        for ($i = 1; $i < count($this->phraseCompressed); $i++) {
            $k = $this->phraseCompressed[$i];
            if ($this->dictionaryUncompress[$k]) {
                $entry = $this->dictionaryUncompress[$k];
            } else {
                if ($k === $this->dictionarySize) {
                    $entry = $w . $w[0];
                } else {
                    return null;
                }
            }
            $result .= $entry;
            $this->dictionaryUncompress[$this->dictionarySize++] = $w + $entry[0];
            $w = $entry;
        }
        return $result;
    }
}

$compressor = new ZCompre();
$phrase = 'TO BE OR NOT TO BE OR TO BE OR NOT AT ALL';
print "Phrase to be compressed, to: " . $phrase;

$comped = $compressor->compress($phrase);
print "\nCompressed phrase returned is " . $comped;

$uncomped = $compressor->decompress($comped);
print "\nUncompressed phrase is " . $uncomped;

print "\n";
?>