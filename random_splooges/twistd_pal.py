from twisted.web import server, resource
from twisted.internet import reactor
from pycallgraph import PyCallGraph
from pycallgraph.output import GraphvizOutput

"""
Palindrome checking single purpose webserver using twisted.
	Marlon v/d Linde
"""
class JeanJayResource(resource.Resource):
    isLeaf = True

    def is_palindrome(self, string):
	reversed = string[::-1]
	if reversed == string:
	    return True
	else:
	    return False
    
    def render_GET(self, request):
	pal = None
        request.setHeader("content-type", "text/plain")
	if 'pal' in request.args:
	    pal = str(request.args['pal'][0])
	    resx = self.is_palindrome(pal)
	    if resx:
		return str(resx)
	return "False"

reactor.listenTCP(8088, server.Site(JeanJayResource()))
with PyCallGraph(output=GraphvizOutput()):
	reactor.run()
