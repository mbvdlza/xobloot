<?php

$my_img = imagecreate( 400, 70 );
$background = imagecolorallocate( $my_img, 0, 0, 0 );
$text_colour = imagecolorallocate( $my_img, 206, 122, 20 );
$text_colour2 = imagecolorallocate( $my_img, 160, 250, 0 );
$line_colour = imagecolorallocate( $my_img, 108, 109, 102 );
$second_color = imagecolorallocate( $my_img, 80,66,209);

$uptime = `uptime`;

imagestring( $my_img, 2, 10, 30, $uptime, $text_colour );

imagesetthickness ( $my_img, 10 );

imageline( $my_img, 10, 20, 390, 20, $line_colour );
imageline( $my_img, 10, 50, 390, 50, $second_color );

header( "Content-type: image/png" );
imagepng( $my_img );
imagecolordeallocate( $line_color );
imagecolordeallocate( $text_color );
imagecolordeallocate( $background );
imagedestroy( $my_img );
?>
