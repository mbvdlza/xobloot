<?



class Foo {

  public $splitReportingArray; 
 

  /**
     * Brain numbing multidimensional array to single-dimensional array seperator.
     * @param array $bigNestedArray
     * @see splitReportingArray
     */
    private function array_breaker($bigNestedArray) {
        foreach ($bigNestedArray as $arrayKey => $arrayItem) {
            if (is_array($arrayItem)) {
                $this->array_breaker($arrayItem);
                unset($bigNestedArray[$arrayKey]);
            }

        }

        if (!empty($bigNestedArray)) {
            $this->splitReportingArray[] = $bigNestedArray;
        }
    }

}

?>
