# marlon van der linde
# Demo: magic functions

class Secret(object):
    def __init__(self, symbol):
        self.symbol = symbol

    def __str__(self):
        return hex(ord(self.symbol))

    def __get__(self):
        return hex(ord(self.symbol))

    def __repr__(self):
        return hex(ord(self.symbol))

    def __add__(self, secret):
        def mkint(char):
            return int(ord(char))
        if isinstance(secret, Secret):
            return hex(mkint(secret.symbol) + mkint(self.symbol))
        else:
            return False







foo = Secret("Y")
print foo

bar = Secret("!")
print bar

print foo + bar


