package biz.rightshift.engineroom.engine.api.logger;

import java.util.ArrayList;
import java.util.List;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Used to functionally test the logger API in our script engine code.
 * @author marlon
 */
public class LoggerClientImplFuncTest {

    private final static List<LoggerClient> clients = new ArrayList<LoggerClient>();
    
    public static void main(final String[] args) throws ScriptException {
        
        clients.add(new LoggerClientImpl("tenantID8"));
        clients.add(new LoggerClientImpl("tenantID11"));
        
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");

        engine.put("loggerClient0", clients.get(0));
        engine.put("loggerClient1", clients.get(1));

        System.out.println("------------ LOGGING ----------------");
        engine.eval("var x = loggerClient0.error('Same Tenant - Testing an informational output', 'HH.js');");
        engine.eval("var x = loggerClient0.error('Same Tenant - Rotten', 'HH.js');");
        
        engine.eval("var x = loggerClient1.info('Best Band Ever Detected. Differing Scripts **', 'Abba.js');");
        engine.eval("var x = loggerClient0.info('Best Song Detected. Differing Scripts **', 'Waterloo.js');");
        
        engine.eval("var x = loggerClient1.info('test info on same script as the next error', 'gummy.js');");
        engine.eval("var x = loggerClient1.error('Error on the same script as the previous info', 'gummy.js');");
        System.out.println("------------ LOGGING ends----------------");
    }
}
