# marlon van der linde marlon@kbye.co.za
# Demonstrating callbacks

class Posint(object):
    """A class representing a positive number.
    It's almost useless for anything other than this demo.
    """
    cache = 0
    sc = None
    fc = None

    def __init__(self, successCallback, failCallback):
        """Not very elegant to set the callbacks here, but..."""
        if hasattr(successCallback, '__call__') and \
                hasattr(failCallback, '__call__'):
            self.sc = successCallback
            self.fc = failCallback
        else:
            raise ValueError('callbacks must be callable') 

    def set(self, aVal=None):
        """Set a value for this mutable posint"""
        self.cache = 0 if (aVal is None) else aVal
        if aVal < 0:
            self.fc()

    def add(self, posint):
        """Add another posint to this posint"""
        if not isinstance(posint, self.__class__):
            raise ValueError('can not work on non Posint type')
        self.sc()
        return self.cache + posint.cache

