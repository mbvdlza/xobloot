from posint import Posint

# two simple handlers that will be passed as callbacks later
def errorHandler():
    raise RuntimeError("bad bad bad!")

def successHandler():
    print "Good!"

# create two Posint objects, pass the handlers.
n = Posint(successHandler, errorHandler)
y = Posint(successHandler, errorHandler)

# this will succeed, two nice old Posints
n.set(40)
y.set(100)
print y.add(n)

# this will fail, because negative values are not allowed
n.set(10)
y.set(-90)
print n.add(y)

# this will also fail, because we can't add an integer to Posint
n.set(50)
print n.add(99)

