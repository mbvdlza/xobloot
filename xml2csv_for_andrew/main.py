import xml.etree.ElementTree as ET
import csv
import datetime

tree = ET.parse("dat/CategoriesList.xml")
root = tree.getroot()
timef = str(datetime.datetime.now()).split('.')[0]
filename = "dat/out_%s.csv" % (timef, )
out_data = open(filename, 'w')
csvwriter = csv.writer(out_data)

# inits
rows = []

# iterate and maintain Category as a Parent
for category in root.findall('.//Category'):

    row = []
    for description in category.findall('.//Description'):
        if description.attrib['langid'] == '1':
            row.append(description.attrib['ID'])

    for keyword in category.findall('.//Keywords'):
        if keyword.attrib['langid'] == '1':
            row.append(keyword.attrib['ID'])

    for name in category.findall('.//Name'):
        if name.attrib['langid'] == '1':
            value = ""
            if "Value" in name.attrib:
                value = name.attrib['Value']

                # only cater for a non-empty, there are ParentCategory elements with empties
                row.append(value)

    rows.append(row)
    csvwriter.writerow(row)

print rows
out_data.close()
