<?php

/**
 * A super vanilla twitter class with url shortening included (thanks to bit.ly)
 * @author Marlon B van der Linde
 * marlon@kbye.co.za
 * Somewhere 2010
 */
class Kbye_Twitter {

    private $twitter_password = null;
    private $twitter_username = null;
    private $twitter_api = null;
    private $bit_login = null;
    private $bit_key = null;
    private $bitlyapi = null;
    const bitly_version = '2.0.1';
    public  $debug = null;

    /**
     * Constructor taking the arguments:
     * @param $twit_username twitter username
     * @param $twit_password twitter password
     * @param $bit_login bit.ly login username
     * @param $bit_key bit.ly API key (not password)
     * @param $twit_api optional address for the twitter api
     * @param $bit_api optional address for the bit.ly api
     */
    function __construct($twit_username, $twit_password, $bit_login, $bit_key, 
                            $twit_api='twitter.com/statuses/update.xml', $bit_api='api.bit.ly') {
        $this->bitlyapi = $bit_api;
        $this->twitter_api = $twit_api;
        $this->twitter_password = $twit_password;
        $this->twitter_username = $twit_username;
        $this->bit_login = $bit_login;
        $this->bit_key = $bit_key;
        $this->debug = 0;   // default off
        header("X-Twitter-Client: KbyeTwitter");
    }

    // destructor
    function __destruct() {
    }

    
    /**
     * Turn debug on or off
     * @param bool
     */
    function debug($bool) {
        $this->debug = (bool)$bool;
    }

    
    /**
     * Friends & user timelines, aka "home" timeline
     * @param $count Number of statusses te fetch
     */
    function timeline($count) {
        
        $old_api = $this->twitter_api;     // for home timeline, it's different than for updates
        $this->twitter_api = 'api.twitter.com/1/statuses/home_timeline.xml';

        $curly = curl_init();
        curl_setopt($curly, CURLOPT_URL, "http://$this->twitter_api");
        curl_setopt($curly, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curly, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curly, CURLOPT_POST, 1);
        curl_setopt($curly, CURLOPT_POSTFIELDS, "count=$count");
        curl_setopt($curly, CURLOPT_USERPWD, "$this->twitter_username:$this->twitter_password");
        $rsp = curl_exec($curly);
        curl_close($curly);
         
        $xml = simplexml_load_string($rsp);

        //if($this->debug) {
        //    print "<br />Curl response XML: <pre>";
        //    var_dump($xml);
        //    print "</pre>";
        //}

        $status_array = array();
        $i = 0;
        foreach($xml->status as $status) {
            $status_array[$i]['screen_name']    = (string)$status->user->screen_name;
            $status_array[$i]['text']           = (string)$status->text;
            $status_array[$i++]['created_at']   = (string)$status->created_at;
        }

        // revert back the update api
        $this->twitter_api = $old_api;
        return $status_array;
    }


    /**
     * Post a new update/status to twitter
     * @param $message string to post (will be capped to 140 chars after shorten url)
     */
    public function update($message) {
        if($this->debug) print "<br />Entering function update()";
        // check for url and send to urlpiston()
        $urlreg3 = "@(https?://([-\w\.]+)+(:\d+)?(/([-#\w/_\.]*(\?\S+)?)?)?)@";   // so far, so good

        preg_match_all($urlreg3, $message, $links);

        if($this->debug) {
            print "<br />Message: $message";
            print "<br />Link Matches: <pre>";
            print_r($links);
        }

        if($links[0]) {
            $shorts = array();
            foreach($links[0] as $link) {
                //$shorts[] = $this->urlpiston($link);
                $message = str_ireplace($link, $this->urlpiston($link), $message);
            }
            foreach($shorts as $l) {
            }
        }

        if($this->debug) {
            print "<br />Shortened Message String Final: " . $message;
        }

        //return true;  // uncomment this to disable

        if(strlen($message) > 140) {
            return array(
                'status' => 'toolong',
                'message' => $message,
                );
        }

        if(!$this->debug) {  // if debug, don't send tweet!
            $curly = curl_init();
            curl_setopt($curly, CURLOPT_URL, "http://$this->twitter_api");
            curl_setopt($curly, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curly, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curly, CURLOPT_POST, 1);
            curl_setopt($curly, CURLOPT_POSTFIELDS, "status=$message");
            curl_setopt($curly, CURLOPT_USERPWD, "$this->twitter_username:$this->twitter_password");
            $rsp = curl_exec($curly);
            curl_close($curly);
        } else {
            print "<br />Skipping real curl... faking it";
            $rsp = "FOOBAR";
        }
          
        if($this->debug) {
            print "<br />Curl response: <pre>";
            var_dump($rsp);
            print "</pre>";
        }

        if(strlen($rsp)<1) {
            if($this->debug) print "<br />Returning False";
            return array(
                'status' => 'notsent',
                'message' => $message,
                );
        } else {
            if($this->debug) print "<br />Returning True";
            return array(
                'status' => 'sent',
                'message' => $message,
            );
        }
    }


    // compress url using bit.ly api
    public function urlpiston($url) {
        if($this->debug) print "<br />Entering function urlpiston() with url: $url";
        //create the URL
        $url = urlencode($url);
        $bitly =
        'http://'.$this->bitlyapi.'/shorten?version='.self::bitly_version.'&longUrl='
                .$url.'&login='.$this->bit_login.'&apiKey='.$this->bit_key.'&format=xml';

        if($this->debug) print "<br />Bit.ly shorten call we have constructed: " . $bitly;
    
        $response = @file_get_contents($bitly);
    
        $xml = simplexml_load_string($response);
        
        if($this->debug) {
            print "<br />Bit.ly response XML:<br /><pre>";
            var_dump($xml);
            print "</pre>";
        }

        return 'http://bit.ly/'.$xml->results->nodeKeyVal->hash;
    }
      
    /**
     * Used for testing. A tinyURL version.
     * No stats available for url's shortened with this
     * @param $url long url to shorten
     * @return short version of url
     */
    public function urlpiston2($url) {
        if($this->debug) print "<br />Entering function urlpiston2() with url: $url"; 
        $tiny = 'http://tinyurl.com/api-create.php?url=';     
        $tinyhandle = fopen($tiny.urlencode(trim($url)), "r");     
        $tinyurl = fread($tinyhandle, 26);  // 26, one more than tiny's 25, incase of upgrades
        if($this->debug) print "<br />Finished TINY'ing the url to $tinyurl";
        fclose($tinyhandle);     
        return $tinyurl;
    }

    
    /**
     * Retrieves stats from bitly for the shorturl supplied
     * @param $shorturl string
     * @return array of stats
     */
    public function bitlystats($shorturl) {
         if($this->debug) print "<br />Entering function bitlystats() for shorturl: $shorturl";
        $url = urlencode($shorturl);
        $bitly =
        'http://'.$this->bitlyapi.'/stats?version='.self::bitly_version.'&shortUrl='
                .$url.'&login='.$this->bit_login.'&apiKey='.$this->bit_key.'&format=xml';

        if($this->debug) print "<br />Bit.ly stats call we have constructed: " . $bitly;
    
        $response = @file_get_contents($bitly);
    
        $xml = simplexml_load_string($response);
        if($this->debug) {
            print "<br />Bit.ly stats response XML:<br /><pre>";
            var_dump($xml);
            print "</pre>";
        }
        
        if($xml->errorCode > 0) return (string)$xml->errorMessage;
 
        $stats['hash'] = (string)$xml->results->hash;
        $stats['userclicks'] = (int)$xml->results->userClicks;
        $stats['clicks'] = (int)$xml->results->clicks;

        return $stats;
    }


}


?>
