'use strict';

/**
 * Compares the response of a recommendations endpoit query to the training data file:
 * Ensures that no inStock:0 items are in fact, recommended.
 */

var Client = require('node-rest-client').Client;
var client = new Client();

/**
 * The location of the training data file to test against.
 * @type {string}
 */
const TRAIN_DATA = './training-data.json';

const LOGP = '##';

/**
 * Returns a repeated string string.
 * @param {int} n times to repeat string.
 * @returns {string}
 */
String.prototype.r = function(n) {
    return (
        new Array(n + 1)
    ).join(this);
};

function log(msg, decorate) {
    decorate = (decorate > 0) ? decorate : 2;
    console.log(new Date().toString() + ' : ' + LOGP.r(decorate) + ' - ' + parseString(msg));
}

/**
 * HTTP options for the request.
 */
var http_opts = {
    headers: {
        "Content-Type": "application/json"
    }
};

/**
 * int main void :P
 */
function main() {
    let trainData = require(TRAIN_DATA);
    let users = trainData.users;
    let items = trainData.items;

    let doNcount = users.length - 1;
    let recsCount = 3;

    log('Starting', 4);
    for (let i = 1; i < doNcount; i++) { // iterate users

        // not going to check each and every one...
        if (users[i].id % 2) {
            let uid = users[i].id;
            log('uid: ' + uid);

            let endp = 'http://dockerx:32869/vX/someRecsEndpoint?placeyplacey=a_sss&user_id=' +
                parseInt(uid) +'&count=' +
                recsCount;

            client.get(endp, http_opts, function (data, response) {
                for (let z = 0; z < data.data.recommendations.length; z++) {
                    let sku = data.data.recommendations[z].sku;
                    for (let y = 0; y < items.length; y++) {
                        let item = items[y];

                        if (item.id === sku) {
                            if (item.properties.inStock !== 1) {    // if something is not in stock...
                                console.log(LOGP.r(2) +
                                    '  Verifying user_id: ' + uid + ' sku: ' + sku + ' --> stock: ' +
                                    item.properties.inStock + ' -- failed --');
                                throw new EvalError('STOCK MISMATCH');
                            } else {
                                console.log(LOGP.r(2) +
                                    '  Verifying user_id: ' + uid + ' sku: ' + sku + ' --> stock: ' +
                                    item.properties.inStock);
                            }
                        }
                    }

                }
            });

       }

    }

}

main();
